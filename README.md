# Belcarra IOT_Demo

This repository contains Belcarra USBLAN demonstration Windows drivers 
for IoT devices.

See the Windows directory for two kits:

        - Win78 - kits for older versions of Windows
        - Win10 - kit for Windows 10


*N.B. The demonstration drivers are limited to 30 minutes of use. Replugging the device will restart
the 30 minute timer.

